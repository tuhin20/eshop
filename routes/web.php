<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('addproduct','ProductController@addproduct');
Route::post('/saveproduct','ProductController@saveproduct');
Route::get('/viewproduct','ProductController@viewproduct');
Route::get('/dashboard','ProductController@dashboard');

//customer add start
Route::resource('/customer','CustomerController');
Route::get('/showcustomer','CustomerController@showcustomer');
Route::get('/editcustomer/{id}','CustomerController@editcustomer');
Route::get('/deletecustomer/{id}','CustomerController@deletecustomer');
Route::post('/updatecustomer/{id}','CustomerController@updatecustomer');


Route::get('addcategory','CategoryController@addcategory');
Route::post('savecategory','CategoryController@savecategory');
Route::get('viewcategory','CategoryController@viewcategory');
Route::get('/editcategory/{id}','CategoryController@editcategory');
Route::get('/deletecategory/{id}','CategoryController@deletecategory');
//Route::post('/savecustomer','CustomerController@savecustomer');

