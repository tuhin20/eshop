<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>UBold - Responsive admin/assets/ Dashboard Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin/assets/ theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('admin/assets/images/favicon.ico')}}">

    <!-- plugin css -->
    <link href="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet" type="text/css" />

    <!-- App css -->
    <link href="{{asset('admin/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

</head>

<body>

    <!-- Begin page -->
    <div id="wrapper">

        <!-- Topbar Start -->
        @include('admin.layout.navar')
        <!-- end Topbar -->

        <!-- ========== Left Sidebar Start ========== -->
        <div class="left-side-menu">
            @include('admin.layout.leftsidevar')
        </div>

        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="row">
                <div class="col-8">
                    <div class="card-box">

                        <form  method="POST" action="{{url('/updatecustomer/'.$customer->id)}}" enctype="multipart/form-data" >
                            @csrf
                            <div>
                                <label>Cutomer Name * :</label>
                                <input type="text" value="{{$customer->name}}" class="form-control" name="name" required="">
                            </div>
                            <div>
                                <label>Customer Email * :</label>
                                <input type="Email" value="{{$customer->email}}" class="form-control" name="email"  required="">
                            </div>
                            <div>
                                <label>Customer Phone * :</label>
                                <input type="text" value="{{$customer->phone}}" class="form-control" name="phone"  required="">
                            </div>
                            <div>
                                <label>Customer Image * :</label>
                                <input type="file" value="{{$customer->image}}" class="form-control" name="image"required="">
                            </div>
                            <div>
                                <label>Customer Address * :</label>
                                <input type="text" value="{{$customer->address}}" class="form-control" name="address" id="image" required="">
                            </div>
                            <div >
                                <select class="form-control" required="">
                                    <option value="">Choose..</option>
                                    <option value="press">Published</option>
                                    <option value="net">Unpublished</option>

                                </select>
                            </div>
                            <div class="form-group mb-0">
                                <input type="submit" class="btn btn-success" value="Validate">
                            </div>

                        </form>
                    </div>





                </div>
            </div>
        </div>

    </div> <!-- content -->

    <!-- Footer Start -->
    @include('admin.layout.footer')
    <!-- end Footer -->

</div>

<!-- ============================================================== -->
<!-- End Page content -->
<!-- ============================================================== -->


</div>
<!-- END wrapper -->

<!-- Right Sidebar -->
<div class="right-bar">
    <div class="rightbar-title">
        <a href="javascript:void(0);" class="right-bar-toggle float-right">
            <i class="dripicons-cross noti-icon"></i>
        </a>
        <h5 class="m-0 text-white">Settings</h5>
    </div>
    <div class="slimscroll-menu">
        <!-- User box -->
        <div class="user-box">
            <div class="user-img">
                <img src="{{asset('admin/assets/images/users/user-1.jpg')}}" alt="user-img" title="Mat Helme" class="rounded-circle img-fluid">
                <a href="javascript:void(0);" class="user-edit"><i class="mdi mdi-pencil"></i></a>
            </div>

            <h5><a href="javascript: void(0);">Geneva Kennedy</a> </h5>
            <p class="text-muted mb-0"><small>admin/assets/ Head</small></p>
        </div>

        <!-- Settings -->
        <hr class="mt-0" />
        <h5 class="pl-3">Basic Settings</h5>
        <hr class="mb-0" />

        <div class="p-3">
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox1" type="checkbox" checked>
                <label for="Rcheckbox1">
                    Notifications
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox2" type="checkbox" checked>
                <label for="Rcheckbox2">
                    API Access
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox3" type="checkbox">
                <label for="Rcheckbox3">
                    Auto Updates
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-2">
                <input id="Rcheckbox4" type="checkbox" checked>
                <label for="Rcheckbox4">
                    Online Status
                </label>
            </div>
            <div class="checkbox checkbox-primary mb-0">
                <input id="Rcheckbox5" type="checkbox" checked>
                <label for="Rcheckbox5">
                    Auto Payout
                </label>
            </div>
        </div>

        <!-- Timeline -->
        <hr class="mt-0" />
        <h5 class="pl-3 pr-3">Messages <span class="float-right badge badge-pill badge-danger">25</span></h5>
        <hr class="mb-0" />


    </div> <!-- end slimscroll-menu-->
</div>
<!-- /Right-bar -->

<!-- Right bar overlay-->
<div class="rightbar-overlay"></div>

<!-- Vendor js -->
<script src="{{asset('admin/assets/js/vendor.min.js')}}"></script>

<!-- Plugins js-->
<script src="{{asset('admin/assets/libs/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('admin/assets/libs/jquery-vectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

<!-- Dashboard 2 init -->
<script src="{{asset('admin/assets/js/pages/dashboard-2.init.js')}}"></script>

<!-- App js-->
<script src="{{asset('admin/assets/js/app.min.js')}}"></script>

</body>
</html>