<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addcategory()
    {
        $categories=Category::all();
          return view('admin.addcategory',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function savecategory(Request $request)
{

    $category=new Category();
    $category->name=$request->name1;

    $category->parentid=$request->parentid;
    $category->Save();
   // dd($category); 
  /*    echo "<pre>";
    print_r($category);die();  */
    return  redirect('addcategory');

   }
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function viewcategory(Category $category)
    {
       $parentid=Category::all();
       return view('admin.viewcategory',compact('parentid'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function editcategory($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function deletecategory($id)
    {
         $category = Category::find($id);
 $category->delete();
// Load user/createOrUpdate.blade.php view
    return back();
    }
}
