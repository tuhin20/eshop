<?php

namespace App\Http\Controllers;

use App\Models\Dev;
use Illuminate\Http\Request;

class DevController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "hello Laravel";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addproduct()
    {
        return view('admin.addproduct');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Dev  $dev
     * @return \Illuminate\Http\Response
     */
    public function show(Dev $dev)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Dev  $dev
     * @return \Illuminate\Http\Response
     */
    public function edit(Dev $dev)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Dev  $dev
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dev $dev)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Dev  $dev
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dev $dev)
    {
        //
    }
}
