<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
// echo "string";
    return view('admin.customer');
}
public function create()
{
// echo "string";
    return view('admin.customer');
}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/


/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
    $customer=new Customer();
    $customer->name=$request->name;

    $customer->email=$request->email;
    $customer->phone=$request->phone;

    $customer->image=$request->image;
    $customer->address=$request->address;
//  $product->division_id=$request->division_id;
/* echo "<pre>";
print_r($product);die();*/
$customer->Save();   
return back();
}

/**
* Display the specified resource.
*
* @param  \App\Customer  $customer
* @return \Illuminate\Http\Response
*/
public function showcustomer(Customer $customer)
{
    $customer=Customer::all();
//dd($customer);
    return view('admin.viewcustomer',compact('customer'));
}

/**
* Show the form for editing the specified resource.
*
* @param  \App\Customer  $customer
* @return \Illuminate\Http\Response
*/
public function editcustomer($id)
{
    $customer = Customer::find($id);
// Load user/createOrUpdate.blade.php view
    return View('admin.editcustomer',compact('customer'));
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  \App\Customer  $customer
* @return \Illuminate\Http\Response
*/
public function update(Request $request, Customer $customer)
{
//
}

/**
* Remove the specified resource from storage.
*
* @param  \App\Customer  $customer
* @return \Illuminate\Http\Response
*/
public function deletecustomer($id)
{
 $customer = Customer::find($id);
 $customer->delete();
// Load user/createOrUpdate.blade.php view
    return back();
}
public function updatecustomer(Request $request, $id)
{
    $customer = Customer::find($id);
    $customer->name=$request->name;

    $customer->email=$request->email;
    $customer->phone=$request->phone;

    $customer->image=$request->image;
    $customer->address=$request->address;
//  $product->division_id=$request->division_id;
/* echo "<pre>";
print_r($product);die();*/
$customer->save();   
return back();
}

}
